include NeApiWrapper

module Api
  module V1
    class NeApiController < ApplicationController
      protect_from_forgery

      def ne_auth_execute
        auth_response = NeApiWrapper.ne_auth_execute(params[:uid], params[:state])
        company_user  = OauthService.register(auth_response)

        response = {
          company: company_user.keys.first,
          user: company_user[company_user.keys.first]
        }

        render json: { status: 'SUCCESS', message: 'loaded posts', data: response }
      end

      def execute
        user = User.find(params[:user_id].to_i)

        response = api_execute_with_params(
          user,
          params[:endpoint],
          eval(params[:params])
        )

        render json: { status: 'SUCCESS', message: 'loaded posts', data: response }
      end
    end
  end
end
