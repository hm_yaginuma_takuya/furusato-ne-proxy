module Api
  module V1
    class UsersController < ApplicationController
      protect_from_forgery

      def index
        users = User.all
        render json: { status: 'SUCCESS', message: 'loaded posts', data: users }
      end
    end
  end
end
