class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :ne_company_id_hash, unique: true, null: false
      t.string :main_function_id_hash, null: false
      t.string :name, null: false
      t.string :name_kana, null: false

      t.timestamps
    end
  end
end
