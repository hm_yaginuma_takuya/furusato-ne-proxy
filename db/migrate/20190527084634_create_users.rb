class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.references :company, foreign_key: true, null: false
      t.string :uid, unique: true, null: false
      t.integer :pic_id
      t.string :pic_ne_id
      t.string :pic_name
      t.string :pic_kana
      t.string :pic_mail_address
      t.integer :user_role_id
      t.string :access_token, unique: true, null: false
      t.string :refresh_token, unique: true, null: false

      t.timestamps
    end
  end
end
