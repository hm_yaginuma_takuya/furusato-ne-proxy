Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      post 'auth', to: 'ne_api#ne_auth_execute'
      post 'execute', to: 'ne_api#execute'
      resources :users
    end
  end
end
