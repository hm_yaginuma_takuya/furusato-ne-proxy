require 'net/http'
require 'json'

# NeAPIを扱うラッパーモジュール
# トークンの更新等を包括的に行う
# 参考: https://github.com/infinity-octaver/ne_api
# @note NeAPIクラスを使用してAPIを叩く場合、
#   APIレスポンスがエラーならExceptionとしてエラー内容が出力される
# @note Exceptionは共通処理(ApplicationController.render_error)でログに吐く等の処理し、
#   トークンの更新はensureブロックで行う
module NeApiWrapper
  CLIENT_ID     = ENV['NE_API_CLIENT_ID']
  CLIENT_SECRET = ENV['NE_API_CLIENT_SECRET']
  CALLBACK_URI  = '/callback'

  # 使用しているエンドポイントのパス
  # gemの仕様上、下記のようにapi_v1以降のパスの/を_にして渡す
  PATH_API_LOGIN_COMPANY       = 'login_company_info'
  PATH_API_LOGIN_USER          = 'login_user_info'
  PATH_API_SHOP_SEARCH         = 'master_shop_search'
  PATH_API_ORDER_SEARCH        = 'receiveorder_base_search'
  PATH_API_ORDER_DETAIL_SEARCH = 'receiveorder_row_search'
  PATH_API_SUPPLIER_SEARCH     = 'master_supplier_search'
  PATH_API_ORDER_RECEIPTED     = 'receiveorder_base_receipted'
  PATH_API_ORDER_SHIPPED       = 'receiveorder_base_shipped'
  PATH_API_ORDER_BULKUPDATE    = 'receiveorder_base_bulkupdate'

  # 汎用SQL
  # NOTE: 隠しAPIで何でもSQLが実行できてしまうので無闇やたらに使わないこと
  # エンドポイントが提供されていないところにだけ仕方なく利用する
  # @see https://docs.google.com/spreadsheets/d/1BcZTGHAitbKG825HELFcKzZNAuqYaFuGJMInPbkWLe4/edit#gid=0
  PATH_API_ALL_EXECUTE   = '/api_v1_Internal_sql/execute'

  # APIレスポンスのパラメーターで、よく使う値を定数化
  PARAMS_RESPONSE_SUCCESS       = 'success'
  PARAMS_RESPONSE_RESULT        = 'result'
  PARAMS_RESPONSE_ACCESS_TOKEN  = 'access_token'
  PARAMS_RESPONCE_REFRESH_TOKEN = 'refresh_token'
  PARAMS_RESPONSE_DATA          = 'data'
  PARAMS_RESPONSE_COUNT         = 'count'

  # curlのタイムアウト設定
  CURL_TIME_OUT = 30

  module_function

  # NE認証を実行する
  # @return [hash] APIレスポンス
  def ne_auth_execute(uid, state)
    auth = NeAPI::Auth.new(redirect_url: CALLBACK_URI)
    auth.ne_auth(uid, state, CLIENT_ID, CLIENT_SECRET)
  end

  # APIを実行する。意図的にトークンの更新を行わない
  # @note 新規企業やユーザーを登録する際に使用する
  def api_execute(access_token, refresh_token, end_point_path)
    NeAPI::Master.new(access_token: access_token, refresh_token: refresh_token).send(end_point_path)
  end

  # NeAPIオブジェクトを返却する
  # @param [User] user APIを実行するユーザーのUserオブジェクト
  def ne_api_client(user)
    NeAPI::Master.new(access_token: user.access_token, refresh_token: user.refresh_token)
  end

  # APIを実行してトークンを更新する
  # @param [User] user APIを実行するユーザーのUserオブジェクト
  # @param [String] end_point_path 実行するAPIのエンドポイント
  def api_execute_and_update(user, end_point_path)
    begin
      @master = ne_api_client(user)
      response = @master.send(end_point_path)

      response
    ensure
      update_token user, @master.access_token, @master.refresh_token
    end
  end

  # パラメーターが必要なAPIを実行してトークンを更新する
  # @note SDKの実装漏れっぽい問題でcreateをエンドポイントに持つものだけmethod_missingを使うとコケるのでこの形で対応
  # @param [User] user APIを実行するユーザーのUserオブジェクト
  # @param [String] end_point_path 実行するAPIのエンドポイント
  # @param [Hash] params パラメーター
  def api_execute_with_params(user, end_point_path, params)
    begin
      @master = ne_api_client(user)
      unless (models = /^(.*)_.*$/.match(end_point_path.to_s))
        # LoggerUtil.output(
        #   user.company_id,
        #   user.id,
        #   'info',
        #   "存在しないNE APIのエンドポイントにアクセスされました。エンドポイント: #{end_point_path.to_s}"
        # )

        return Hash.new
      end

      model    = models.captures.first.to_sym
      method   = end_point_path.to_s.split('_').last
      response = @master.post(
        method: method,
        model: model,
        query: nil,
        fields: nil,
        get_key: nil,
        params: params
      )

      response
    ensure
      update_token(user, @master.access_token, @master.refresh_token)
    end
  end

  # 隠しAPIの実行口
  # SDKだとyamlに定義してあるエンドポイントしか実行できない
  # SDKに隠しAPIのエンドポイントを載せるわけにもいかないのでここはSDKとは別で実装する
  # @note paramsのキーは文字列(String)を使用すること
  def secret_api_execute(user, end_point_path, params = {})
    response = {}
    placeholder_sql = params['placeholder_sql']
    is_select = params['is_select']
    data_type = params['data_type']
    placeholder_values = params['placeholder_values']

    send_data = {
        'access_token' => user.access_token,
        'refresh_token' => user.refresh_token,
        'placeholder_sql' => placeholder_sql,
        'is_select' => is_select,
        'data_type' => data_type,
    }
    send_data.merge!(placeholder_values)

    uri = URI.parse(NeAPI::API_SERVER_HOST+end_point_path)
    http_obj = Net::HTTP.new(uri.host, uri.port)
    if(uri.port == 443)
      http_obj.use_ssl = true
    end
    http_obj.start do |http|
      # リクエストインスタンス生成
      request = Net::HTTP::Post.new(uri.path)
      request.set_form_data(send_data)
      # タイムアウト設定
      http.open_timeout = CURL_TIME_OUT
      http.read_timeout = CURL_TIME_OUT
      # 送信
      response = http.request(request)
    end

    # 実行後はアクセストークンの更新
    result = JSON.parse(response.body)
    update_token(user, result[PARAMS_RESPONSE_ACCESS_TOKEN], result[PARAMS_RESPONCE_REFRESH_TOKEN])

    result
  end

  # トークンを更新するための共通処理
  # @param [User] user APIを実行するユーザーのUserオブジェクト
  # @param [String] access_token アクセストークン
  # @param [String] refresh_token リフレッシュトークン
  def update_token(user, access_token, refresh_token)
    user.access_token  = access_token
    user.refresh_token = refresh_token
    unless user.save
      raise 'アクセストークンとリフレッシュトークンの更新に失敗しました'
    end
  end

  # 認証処理へリダイレクトする
  # @note セッションが切れていた場合等に使用する
  # @note NE未ログインの場合はログイン画面が表示される
  def redirect_to_login
    redirect_to(NeAPI::NE_SERVER_HOST + NeAPI::Auth::SIGN_IN_PATH + "?client_id="+CLIENT_ID+"&client_secret="+CLIENT_SECRET+"&redirect_uri="+CALLBACK_URI)
  end

  # APIのレスポンスが成功か失敗かを判断する汎用メソッド
  # 失敗だった場合にはログに記録しエラーメッセージを返す
  # 成功だった場合には空文字を返す
  # @param [hash]
  # @return [string]
  def get_api_response_error_message(response)
    if response[PARAMS_RESPONSE_RESULT] == PARAMS_RESPONSE_SUCCESS
      return ''
    else
      # AppLogger.error(
      #   "APIの実行に失敗しました。エラーメッセージ: [#{response['code']}]#{response['message']}"
      # )
      return 'アプリケーションエラーが発生しました。操作をやり直してください。'
    end
  end

  # NEAPIからのレスポンスの共通エラー処理
  # 正しくレスポンスを取得できていた時はDataを返す
  def parse_response(response_hash)
    unless response_hash[NeApiService::PARAMS_RESPONSE_RESULT] == NeApiService::PARAMS_RESPONSE_SUCCESS
      error_msg = "APIの実行に失敗しました。エラーメッセージ: [#{response_hash['code']}]#{response_hash['message']}"
      # AppLogger.error(error_msg)
      raise error_msg
    end
    response_hash[NeApiService::PARAMS_RESPONSE_DATA]
  end
end
